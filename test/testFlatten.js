const flatten = require("../flatten");

const nestedArray = [1, [2], [[3]], [[[4]]]];
try{
    const flattenArray= flatten(nestedArray);
    console.log(flattenArray);
}catch(error){
    console.error(error.message);
}
