const filter = require("../filter");

const items=[1, 2, 3, 4, 5, 5];

try{
    const filterArrayIs=filter(items,item => item % 2 ==0 );
    console.log("Filtered array:",filterArrayIs);

}catch(error){
    console.error(error.message);
}