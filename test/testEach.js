const each = require("../each");

const items=[1, 2, 3, 4, 5, 5];

try{
    each(items , (item ,index)=>{
        console.log(`Item ${item} at index ${index}`);
    });
}catch(error){
    console.error(error.message);
}
