const find = require("../find");

const items=[1, 2, 3, 4, 5, 5];

try{

   const itemFound = find(items, item=>item===5);
   console.log("Item Found in The Array:",itemFound);

}catch(error){

    console.error(error.message);
    
}